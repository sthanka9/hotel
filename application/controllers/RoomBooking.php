<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RoomBooking extends CI_Controller {

    function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		//$this->load->library('session');
		//$this->load->model('Car_model', 'Car', TRUE );		
	}
	
	public function index()
	{
		//echo "Venkat Here";exit;
		//$car_id = $this->uri->segment(3);
		//$data["cardetails"] = $this->Car->get_carDetailsById($car_id);
		$data	= '';		
		$this->load->view('admin_template/header1');
		$this->load->view('roombooking/index',$data);
		$this->load->view('admin_template/footer1');
	}
	public function create()
	{
		//echo "Venkat Here";exit;
		//$car_id = $this->uri->segment(3);
		//$data["cardetails"] = $this->Car->get_carDetailsById($car_id);
		$data	= '';		
		$this->load->view('admin_template/header1');
		$this->load->view('roombooking/create',$data);
		$this->load->view('admin_template/footer1');
	}
	public function modify()
	{
		//echo "Venkat Here";exit;
		//$car_id = $this->uri->segment(3);
		//$data["cardetails"] = $this->Car->get_carDetailsById($car_id);
		$data	= '';		
		$this->load->view('admin_template/header1');
		$this->load->view('roombooking/modify',$data);
		$this->load->view('admin_template/footer1');
	}
	public function billgenerate()
	{
		$data	= '';		
		$this->load->view('admin_template/header1');
		$this->load->view('roombooking/billgenerate',$data);
		$this->load->view('admin_template/footer1');
	}
	public function vacant()
	{
		$data	= '';		
		$this->load->view('admin_template/header1');
		$this->load->view('roombooking/vacant',$data);
		$this->load->view('admin_template/footer1');
	}
	public function billpdf()
	{
		$data	= '';		
		$this->load->view('admin_template/header1');
		$this->load->view('roombooking/billpdf',$data);
		$this->load->view('admin_template/footer1');
	}
	
}
