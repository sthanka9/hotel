
			<div class="page-sidebar-wrapper">

				<div class="page-sidebar navbar-collapse collapse">

					<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
						<li class="start active">
							<a href="<?= base_url() ?>Dashboard">
								<i class="icon-home"></i>
								<span class="title">Dashboard</span>
								<span class="selected"></span>
							</a>
						</li>
						<li>
							<a href="javascript:;">
								<i class="icon-film"></i>
								<span class="title">Booking</span>
								<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li>
									<a href="<?= base_url() ?>Checkin">CHECK-IN</a>
								</li>

								<li>
									<a href="<?= base_url() ?>Checkout">CHECK-OUT</a>
								</li>

							</ul>
						</li>
						<li>
							<a href="javascript:;">
								<i class="icon-docs"></i>
								<span class="title">Add List</span>
								<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								
								<li>
									<a href="<?= base_url() ?>Roomtype">Room Types</a>
								</li>
								<li>
									<a href="<?= base_url() ?>Room">Rooms</a>
								</li>

								<li>
									<a href="<?= base_url() ?>Facility">Facilities</a>
								</li>
								
								<li>
									<a href="<?= base_url() ?>AssociatedCompany">Associated Companies</a>
								</li>

							</ul>
						</li>
							<li>
							<a href="<?= base_url() ?>Hotel">
								<i class="icon-bar-chart"></i>
								<span class="title">Hotel</span>
								<span class="arrow "></span>
							</a>
						</li>
						<li>
							<a href="<?= base_url() ?>Reports">
								<i class="icon-bar-chart"></i>
								<span class="title">Reports</span>
								<span class="arrow "></span>
							</a>
						</li>
						<li>
							<a href="<?= base_url() ?>Customers">
								<i class="icon-users"></i>
								<span class="title">Customers</span>
								<span class="arrow "></span>
							</a>
						</li>

						<li>
							<a href="<?= base_url() ?>Employees">
								<i class="icon-users"></i>
								<span class="title">Employee</span>
								<span class="arrow "></span>
							</a>
						</li>

						<li>
							<a href="<?= base_url() ?>Configurations">
								<i class="icon-settings"></i>
								<span class="title">Configuration</span>
							</a>

						</li>





					</ul>
					<!-- END SIDEBAR MENU -->
				</div>
			</div>
			