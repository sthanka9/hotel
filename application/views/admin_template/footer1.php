<div class="page-footer">
			<div class="page-footer-inner">
				2017 &copy; Frontinn.com.
			</div>
			<div class="scroll-to-top">
				<i class="icon-arrow-up"></i>
			</div>
		</div>
			</div>
			<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<!--<script src="js/metronic.js" type="text/javascript"></script>
	<script src="js/layout.js" type="text/javascript"></script>
	<script src="js/demo.js" type="text/javascript"></script>
	<script src="js/bootstrap-multiselect.js" type="text/javascript"></script>-->
	
	<script src="<?= base_url() ?>assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="<?= base_url() ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
	
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"
	 type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!--<script>
		jQuery(document).ready(function () {
			Metronic.init(); // init metronic core componets
			Layout.init(); // init layout
			Demo.init(); // init demo features 

		});
	</script>-->

	<script type="text/javascript">
		$(function () {
			$('#check-in').datetimepicker();
			$('#check-out').datetimepicker();
		});

		/*$(function () {
			$('.multiselect-ui').multiselect({
				includeSelectAllOption: true
			});
		});*/
	
	var room = 1;
function education_fields() {
 
    room++;
    var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
	divtest.setAttribute("class", "form-group1 removeclass"+room);
	var rdiv = 'removeclass'+room;
    //divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class="form-group"> <input type="text" class="form-control" id="Schoolname" name="Schoolname[]" value="" placeholder="School name"></div></div><div class="col-sm-3 nopadding"><div class="form-group"> <input type="text" class="form-control" id="Major" name="Major[]" value="" placeholder="Major"></div></div><div class="col-sm-3 nopadding"><div class="form-group"> <input type="text" class="form-control" id="Degree" name="Degree[]" value="" placeholder="Degree"></div></div><div class="col-sm-3 nopadding"><div class="form-group"><div class="input-group"> <select class="form-control" id="educationDate" name="educationDate[]"><option value="">Date</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option> </select><div class="input-group-btn"> <button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div><div class="clear"></div>';
	divtest.innerHTML = '<div class="col-md-6 nopadding"><div class="form-group"><label class="control-label col-md-3">Room Type</label><div class="col-md-9"><select class="form-control"><option value="Single">Single</option><option value="Double">Double</option><option value="Triple">Triple</option><option value="Quad">Quad</option><option value="Queen">Queen</option><option value="King">King</option><option value="Twin">Twin</option><option value="Double-double">Double-double</option><option value="Studio">Studio</option></select></div></div></div><div class="col-md-6"><div class="form-group"><label class="control-label col-md-3">Room No.</label><div class="col-md-7"><select class="form-control"><option value="123">123</option><option value="234">234</option><option value="345">345</option><option value="456">456</option><option value="567">567</option><option value="765">765</option><option value="235">235</option></select></div><div class="col-md-2 input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div><div class="clear"></div>';
    
    objTo.appendChild(divtest)
}
   function remove_education_fields(rid) {
	   $('.removeclass'+rid).remove();
   }
	
	</script>
	
	<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"
	/>
	
	<script type="text/javascript" src="<?= base_url() ?>assets/picker/jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/picker/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/picker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/picker/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>

<script type="text/javascript">
		$(function () {
			$('#check-in').datetimepicker();
			$('#check-out').datetimepicker();
		});

		
	</script>
	<!-- END JAVASCRIPTS -->
	
	
	
	
	<!-- END JAVASCRIPTS -->
	
	
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js " type="text/javascript"></script>


	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
	
		jQuery(document).ready(function ($) {
			$('#example').DataTable();
			Metronic.init(); // init metronic core componets
			Layout.init(); // init layout
			Demo.init(); // init demo features 
			
			
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>