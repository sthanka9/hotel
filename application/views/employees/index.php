	<div class="container1">
		<div class="page-container">
			<!-- BEGIN SIDEBAR --><!-- END SIDEBAR -->
			   <?php
            $this->load->view('admin_template/leftmenu.php')?>
			<!-- BEGIN CONTENT -->
			
<!-- BEGIN CONTENT -->

			<div class="page-content-wrapper">
				<div class="page-content">



					<div class="row">
						<div class="col-md-12">
							<!-- BEGIN EXAMPLE TABLE PORTLET-->
							<div class="portlet box blue">
								<div class="portlet-title">
									<div class="caption">
										Employees
									</div>

								</div>
								<div class="portlet-body">
									<div class="table-toolbar">
										<div class="row">
											<div class="col-md-6">
												<div class="btn-group">
													<a href="<?= base_url() ?>Employees/create" id="sample_editable_1_new" class="btn green">
														Add New
														<i class="fa fa-plus"></i>
													</a>
												</div>
											</div>

										</div>
									</div>
									<br>

									<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>
													Name
												</th>
												
												<th>
													Designation
												</th>

												<th>
													User ID
												</th>
												<th>
													Creating Date
												</th>
												<th>
													View
												</th>
												<th>
													Edit
												</th>
												<th>
													Status
												</th>

											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Vasmi</td>
												<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> inactive </a>
												</td>

											</tr>
											<tr>
												<td>Veerraju</td>
												<td>Supervisor</td>

												<td> ID</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
												<td>Raju</td>
												<td>Supervisor</td>

												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Pending </a>
												</td>

											</tr>
											<tr>
												<td>Nagendran</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> inactive </a>
												</td>

											</tr>
											<tr>
												<td>Ravi kiran</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
												<td>Sagneeth</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
												<td>Srikanth</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
												<td>Anil kumar</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
												<td>Ravinder</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
												<td>Jagadeesh</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
												<td>Sai Kirshna</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
												<td>Rama Krishna</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
												<td>Satyanarayana</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
												<td>Pavankumar</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
												<td>Kirankumar</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
												<td>Chereen</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
												<td>Sandeep</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
												<td>Srinu</td>
<td>Supervisor</td>
												<td> SA#100</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>

				</div>
			</div>

			</div>
		