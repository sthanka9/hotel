	<div class="container1">
		<div class="page-container">
			<!-- BEGIN SIDEBAR --><!-- END SIDEBAR -->
			   <?php
            $this->load->view('admin_template/leftmenu.php')?>
			<!-- BEGIN CONTENT -->
			
<!-- BEGIN CONTENT -->

			
			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">Profile</div>

						</div>
						
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form class="form-horizontal" action="#">
								<div class="form-body">


									<h3 class="form-section">Address</h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Hotel Name</label>
												<div class="col-md-9">
													<input type="text" class="form-control">

												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Hotel Logo</label>
												<div class="col-md-9">
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<span class="btn btn-default btn-file">
															<input type="file" />
														</span>
														<span class="fileinput-filename"></span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"> Email</label>
												<div class="col-md-9">
													<input type="email" class="form-control">

												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Phone</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>

									</div>

									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Address 1</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Address 2</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Land Mark</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">City</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">State</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Post Code</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">GST No.</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										<!--/span-->

										<!--/span-->
									</div>
									
									
								

									<!--/row-->
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<button class="btn btn-circle blue" type="submit">Submit</button>
													<button class="btn btn-circle default" type="button">Cancel</button>
												</div>
											</div>
										</div>
										<div class="col-md-6">
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					
					</div>

				</div>
			</div>

			</div>
		