	<div class="container1">
		<div class="page-container">
			<!-- BEGIN SIDEBAR --><!-- END SIDEBAR -->
			   <?php
            $this->load->view('admin_template/leftmenu.php')?>
			<!-- BEGIN CONTENT -->
			
<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<div class="page-content">

					<h3 class="page-title">
						Create Hotel</h3>

					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								Hotel Info
							</div>

						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form class="form-horizontal" action="#">
								<div class="form-body">

									<div class="form-group">
										<label class="col-md-3 control-label">Hotel Name</label>
										<div class="col-md-4">
											<div class="input-group">
												<span class="input-group-addon input-circle-left">
													<i class="fa fa-user"></i>
												</span>
												<input type="text" placeholder="Username" class="form-control input-circle-right">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Address</label>
										<div class="col-md-4">
											<div class="input-group">
												<span class="input-group-addon input-circle-left">
													<i class="fa fa-lock"></i>
												</span>
												<input type="password" placeholder="Password" class="form-control input-circle-left">

											</div>
										</div>
									</div>



									<div class="form-group">
										<label class="col-md-3 control-label">Phone</label>
										<div class="col-md-4">
											<div class="input-group">
												<span class="input-group-addon input-circle-left">
													<i class="icon-call-end"></i>
												</span>
												<input type="text" placeholder="Location" class="form-control input-circle-right">

											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Date</label>
										<div class="col-md-4">
											<div class="input-group">
												<span class="input-group-addon input-circle-left">
													<i class="icon-call-end"></i>
												</span>
												<input type="text" placeholder="Location" class="form-control input-circle-right">

											</div>
										</div>
									</div>



								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-circle blue" type="submit">Submit</button>
											<button class="btn btn-circle default" type="button">Cancel</button>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>

				</div>
			</div>

			</div>
		