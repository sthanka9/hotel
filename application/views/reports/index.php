	<div class="container1">
		<div class="page-container">
			<!-- BEGIN SIDEBAR --><!-- END SIDEBAR -->
			   <?php
            $this->load->view('admin_template/leftmenu.php')?>
			<!-- BEGIN CONTENT -->
			
<!-- BEGIN CONTENT -->



			<div class="page-content-wrapper">
				<div class="page-content">



					<div class="row">
						<div class="col-md-12">
							<!-- BEGIN EXAMPLE TABLE PORTLET-->
							<div class="portlet box blue">
								<div class="portlet-title">
									<div class="caption">
										Reports
									</div>
										<div class="btn-group pull-right">
												<button style="color:#333; margin-top:3px;" class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
												</button>
												<ul class="dropdown-menu pull-right">
													<li>
														<a href="javascript:;">
														Print </a>
													</li>
													<li>
														<a href="javascript:;">
														Save as PDF </a>
													</li>
													<li>
														<a href="javascript:;">
														Export to Excel </a>
													</li>
												</ul>
											</div>

								</div>
								<div class="portlet-body">
									<form class="form-horizontal" action="#">

										<div class="form-actions top">
											<div class="row">
												<div class="col-md-2">
													<label class="control-label"> By Room Type</label>
													<select class="form-control">
														<option>Gold</option>
														<option>Platinum</option>
													</select>
												</div>

												<div class="col-md-2">
													<label class="control-label"> By Employee</label>
													<select class="form-control">
														<option>Gold</option>
														<option>Platinum</option>
													</select>
												</div>
												<div class="col-md-2">
													<label class="control-label"> By Referral</label>
													<select class="form-control">
														<option>Gold</option>
														<option>Platinum</option>
													</select>
												</div>

												<div class="col-md-3">
													<label class="control-label"> Todate</label>

													<div class='input-group date' id='datetimepicker6'>
														<input type='text' class="form-control" />
														<span class="input-group-addon">
															<span class="icon-calendar"></span>
														</span>
													</div>
												</div>
												<div class="col-md-3">
													<label class="control-label"> Enddate</label>

													<div class='input-group date' id='datetimepicker7'>
														<input type='text' class="form-control" />
														<span class="input-group-addon">
															<span class="icon-calendar"></span>
														</span>
													</div>
												</div>
												<div class="col-md-2 pull-right text-right">
													<label class="control-label" style="display:block;"> &nbsp; &nbsp; &nbsp;</label>

													<button class="btn green" type="submit">Submit</button>
												</div>

											</div>

										</div>

									</form>


									<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>Customer Name</th>
												<th>Phone</th>
												<th>Room No.</th>
												<th>Bill Amount </th>
												<th>Employee</th>
												<th>Room Type </th>
												<th>Referral</th>
												<th>View Bill</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Taj Kirshna</td>
												<td>9949220002</td>
												<td>123</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Platinum</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Sangeeth</td>
												<td>9949220002</td>
												<td>234</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Platinum</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Gumagummalu</td>
												<td>9949220002</td>
												<td>345</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Taj Kirshna</td>
												<td>9949220002</td>
												<td>123</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Sangeeth</td>
												<td>9949220002</td>
												<td>234</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Platinum</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Gumagummalu</td>
												<td>9949220002</td>
												<td>345</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Taj Kirshna</td>
												<td>9949220002</td>
												<td>123</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Sangeeth</td>
												<td>9949220002</td>
												<td>234</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Gumagummalu</td>
												<td>9949220002</td>
												<td>345</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Taj Kirshna</td>
												<td>9949220002</td>
												<td>123</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Sangeeth</td>
												<td>9949220002</td>
												<td>234</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Gumagummalu</td>
												<td>9949220002</td>
												<td>345</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Taj Kirshna</td>
												<td>9949220002</td>
												<td>123</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Sangeeth</td>
												<td>9949220002</td>
												<td>234</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Gumagummalu</td>
												<td>9949220002</td>
												<td>345</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Taj Kirshna</td>
												<td>9949220002</td>
												<td>123</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Sangeeth</td>
												<td>9949220002</td>
												<td>234</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
											<tr>
												<td>Gumagummalu</td>
												<td>9949220002</td>
												<td>345</td>
												<td> 1000/- </td>
												<td> SA#100</td>
												<td>Glod</td>
												<td>Oyo</td>
												<td><a class="edit" href="javascript:;"> Bill pdf </a></td>
											</tr>
										</tbody>
									</table>


								</div>
							</div>
							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>

				</div>
			</div>


			</div>
		