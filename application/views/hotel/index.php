	<div class="container1">
		<div class="page-container">
			<!-- BEGIN SIDEBAR --><!-- END SIDEBAR -->
			   <?php
            $this->load->view('admin_template/leftmenu.php')?>
			<!-- BEGIN CONTENT -->
			
<!-- BEGIN CONTENT --><div class="page-content-wrapper">
				<div class="page-content">



					<div class="row">
						<div class="col-md-12">
							<!-- BEGIN EXAMPLE TABLE PORTLET-->
							<div class="portlet box blue">
								<div class="portlet-title">
									<div class="caption">
										Hotel list
									</div>

								</div>
								<div class="portlet-body">
									<div class="table-toolbar">
										<div class="row">
											<div class="col-md-6">
												<div class="btn-group">
													<a href="<?= base_url() ?>Hotel/create" id="sample_editable_1_new" class="btn green">
														Add Hotel
														<i class="fa fa-plus"></i>
													</a>
												</div>
											</div>

										</div>
									</div>
									<br>

									<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>
													Hotel Name
												</th>
												<th>
													Manager Name
												</th>
												<th>
													Phone
												</th>

												<th>
													Address
												</th>
												<th>
													Creating Date
												</th>
												<th>
													View
												</th>
												<th>
													Edit
												</th>
												<th>
													Status
												</th>

											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Taj Kirshna</td>
												<td>Vasmi</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> inactive </a>
												</td>

											</tr>
											<tr>
											<td>Taj Kirshna</td>
												<td>Veerraju</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr>
											<td>Taj Kirshna</td>
												<td>Raju</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Pending </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Nagendran</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> inactive </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Ravi kiran</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Sagneeth</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Srikanth</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Anil kumar</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Ravinder</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Jagadeesh</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Sai Kirshna</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Rama Krishna</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Satyanarayana</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Pavankumar</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Kirankumar</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Chereen</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Sandeep</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
											<tr><td>Taj Kirshna</td>
												<td>Srinu</td>
												<td> 9949220002</td>
												<td> Hyderabad</td>
												<td class="center"> 12-04-2016</td>
												<td>
													<a class="view" href="javascript:;">View</a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> Active </a>
												</td>

											</tr>
										</tbody>
									</table>
								
								</div>
							</div>
							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>

				</div>
			</div>

			</div>
		