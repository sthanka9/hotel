	<div class="container1">
		<div class="page-container">
			<!-- BEGIN SIDEBAR --><!-- END SIDEBAR -->
			   <?php
            $this->load->view('admin_template/leftmenu.php')?>
			<!-- BEGIN CONTENT -->
			
<!-- BEGIN CONTENT -->

			<div class="page-content-wrapper">
				<div class="page-content">



					<div class="row">
						<div class="col-md-12">
							<!-- BEGIN EXAMPLE TABLE PORTLET-->
							<div class="portlet box blue">
								<div class="portlet-title">
									<div class="caption">
										Food Order
									</div>

								</div>
								<div class="portlet-body">
									<div class="table-toolbar">
										<div class="row">
											<div class="col-md-6">
												<div class="btn-group">
													<a href="<?= base_url() ?>FoodOrder/create" id="sample_editable_1_new" class="btn green">
														Add Food
														<i class="fa fa-plus"></i>
													</a>
												</div>
											</div>

										</div>
									</div>
									<br>

									<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>
													Room No.
												</th>

												<th>
													Customer
												</th>
												<th>
													Food Type
												</th>
												<th>
													Amount
												</th>
												<th>
													Edit
												</th>
												<th>
													Status
												</th>

											</tr>
										</thead>
										<tbody>
											<tr>
												<td>SA#100</td>
												<td> Anil kumar</td>
												<td> Break fast</td>
												<td> 100.00</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> inactive </a>
												</td>
											</tr>
											<tr>
												<td>SA#100</td>
												<td> Anil kumar</td>
												<td> Break fast</td>
												<td> 100.00</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> inactive </a>
												</td>
											</tr>
											<tr>
												<td>SA#100</td>
												<td> Anil kumar</td>
												<td> Break fast</td>
												<td> 100.00</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> inactive </a>
												</td>
											</tr>
											<tr>
												<td>SA#100</td>
												<td> Anil kumar</td>
												<td> Break fast</td>
												<td> 100.00</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> inactive </a>
												</td>
											</tr>
											<tr>
												<td>SA#100</td>
												<td> Anil kumar</td>
												<td> Break fast</td>
												<td> 100.00</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> inactive </a>
												</td>
											</tr>
											<tr>
												<td>SA#100</td>
												<td> Anil kumar</td>
												<td> Break fast</td>
												<td> 100.00</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> inactive </a>
												</td>
											</tr>
											<tr>
												<td>SA#100</td>
												<td> Anil kumar</td>
												<td> Break fast</td>
												<td> 100.00</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> inactive </a>
												</td>
											</tr>
											<tr>
												<td>SA#100</td>
												<td> Anil kumar</td>
												<td> Break fast</td>
												<td> 100.00</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> inactive </a>
												</td>
											</tr>
											<tr>
												<td>SA#100</td>
												<td> Anil kumar</td>
												<td> Break fast</td>
												<td> 100.00</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> inactive </a>
												</td>
											</tr>
											<tr>
												<td>SA#100</td>
												<td> Anil kumar</td>
												<td> Break fast</td>
												<td> 100.00</td>
												<td>
													<a class="edit" href="javascript:;"> Edit </a>
												</td>
												<td>
													<a class="edit" href="javascript:;"> inactive </a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>

				</div>
			</div>

			</div>
		