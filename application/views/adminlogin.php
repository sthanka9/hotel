<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>Du Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"
    />
    <link href="assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" id="style_components" rel="stylesheet" type="text/css" />
    <link href="assets/css/layout.css" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css"
    />
    <link href="assets/css/grey.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="assets/css/login.css" rel="stylesheet" type="text/css" />
    <!-- END THEME STYLES -->
</head>

<body style="background-color:#f1f3fa !important">
    <!-- BEGIN HEADER -->
    <!-- END HEADER -->
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="container1">
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->

            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content login">
                    <div class="content">
                       
						<?php echo form_open(base_url() . 'welcome/', array('class' => 'form-horizontal', 'id' => 'loginAction')); ?>
                            <h3 class="form-title">Login to your account</h3>
							
							<?php
                if ($this->session->flashdata('msg') != '') {
                    if (is_array($this->session->flashdata('msg'))) {
                        $messages = $this->session->flashdata('msg');
                        ?>
                        <div class="alert alert-danger" style="margin: 0px !important">
                            <button type="button" class="close" data-dismiss="alert"> <span aria-hidden="true">×</span> </button>
                            <div class="msg"><?= $messages['error'] ?></div>
                        </div>
                        <?php
                    }
                }
                ?>
				
				
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span>
                                    Enter any username and password. </span>
                            </div>
                            <label class="control-label visible-ie8 visible-ie9">Username</label>
                            <div style="margin-bottom: 25px" class="input-group">

                                <span class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </span>
                                <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="username">
                            </div>
                            <label class="control-label visible-ie8 visible-ie9">Password</label>
                            <div style="margin-bottom: 25px" class="input-group">

                                <span class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </span>
                                <input id="login-password" type="password" class="form-control" name="password" placeholder="password">
                            </div>
                            <div style="margin-top:10px" class="form-group">
                                <!-- Button -->

                                <div class="col-sm-12 controls">
                                   <button type="submit" name="submit" class="btn btn-primary block full-width m-b">Login</button>
                                </div>
                            </div>
                         <?php echo form_close(); ?>
                    </div>

                </div>
            </div>

        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <!-- END FOOTER -->
    </div>


    <script src="assets/js/jquery.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="assets/js/metronic.js" type="text/javascript"></script>
    <script src="assets/js/layout.js" type="text/javascript"></script>
    <script src="assets/js/demo.js" type="text/javascript"></script>

    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function () {
            Metronic.init(); // init metronic core componets
            Layout.init(); // init layout
            Demo.init(); // init demo features 

        });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>