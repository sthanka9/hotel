	<div class="container1">
		<div class="page-container">
			<!-- BEGIN SIDEBAR --><!-- END SIDEBAR -->
			   <?php
            $this->load->view('admin_template/leftmenu.php')?>
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<div class="page-content ">
					<div class="buttons">
						<button type="button" class="btn-lg btn-primary">Availability Rooms
							<i class="fa fa-bars" style="margin:0px 5px"></i>
							<span class="badge">10</span>
						</button>
						<button type="button" class="btn-lg btn-warning">Occupied Rooms
							<i class="fa fa-indent" style="margin:0px 5px"></i>
							<span class="badge">10</span>
						</button>
						<a href="<?= base_url() ?>FoodOrder/index" class="btn-lg btn-info">Food Orders
							<i class="fa fa-bell" style="margin:0px 5px"></i>
							<span class="badge">10</span>
						</a>
						<a href="<?= base_url() ?>RoomBooking/index" class="btn-lg btn-success">Room Booking
							<i class="fa fa-folder-open" style="margin:0px 5px"></i>
						</a>
					</div>
					<h3 class="page-title">
						<h1>Today's Activities</h1>
					</h3>

					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="dashboard-stat dashboard-stat-light blue-soft">
								<div class="details">
									<div class="number">
										CHECK-IN
										<span class="pull-right">
											<a href="check-in.html">Viewall</a>
										</span>
									</div>
									<div class="desc">
										<div class="row">
											<div class="col-md-8">
												<h1>Room Type</h1>
											</div>
											<div class="col-md-4">
												<h1>Room Number</h1>
											</div>
											<hr>
										</div>
										<div class="row">
											<div class="col-md-8">
												<h1>Single</h1>
											</div>
											<div class="col-md-4">
												<h1>150</h1>
											</div>
											<hr>
										</div>
										<div class="row">
											<div class="col-md-8">
												<h1>Single</h1>
											</div>
											<div class="col-md-4">
												<h1>150</h1>
											</div>
											<hr>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div href="javascript:;" class="dashboard-stat dashboard-stat-light gray-soft">
								<div class="details">
									<div class="number">
										CHECK-OUT
										<span class="pull-right">
											<a href="check-out.html">Viewall</a>
										</span>
									</div>
									<div class="desc">
										<div class="row">
											<div class="col-md-8">
												<h1>Room Type</h1>
											</div>
											<div class="col-md-4">
												<h1>Room Number</h1>
											</div>
											<hr>
										</div>
										<div class="row">
											<div class="col-md-8">
												<h1>Single</h1>
											</div>
											<div class="col-md-4">
												<h1>150</h1>
											</div>
											<hr>
										</div>
										<div class="row">
											<div class="col-md-8">
												<h1>Single</h1>
											</div>
											<div class="col-md-4">
												<h1>150</h1>
											</div>
											<hr>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row dashmrag">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="dashboard-stat dashboard-stat-light yellow-soft">
								<div class="details">
									<div class="number">
										Advanced Booking
										<span class="pull-right">
											<a href="advanced-booking.html">Viewall</a>
										</span>
									</div>
									<div class="desc">
										<div class="row">
											<div class="col-md-8">
												<h1>Room Type</h1>
											</div>
											<div class="col-md-4">
												<h1>Room Number</h1>
											</div>
											<hr>
										</div>
										<div class="row">
											<div class="col-md-8">
												<h1>Single</h1>
											</div>
											<div class="col-md-4">
												<h1>150</h1>
											</div>
											<hr>
										</div>
										<div class="row">
											<div class="col-md-8">
												<h1>Single</h1>
											</div>
											<div class="col-md-4">
												<h1>150</h1>
											</div>
											<hr>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div href="javascript:;" class="dashboard-stat dashboard-stat-light red-soft">
								<div class="details">
									<div class="number">
										Cancellation
										<span class="pull-right">
											<a href="cancellation-room.html">Viewall</a>
										</span>
									</div>
									<div class="desc">
										<div class="row">
											<div class="col-md-8">
												<h1>Room Type</h1>
											</div>
											<div class="col-md-4">
												<h1>Room Number</h1>
											</div>
											<hr>
										</div>
										<div class="row">
											<div class="col-md-8">
												<h1>Single</h1>
											</div>
											<div class="col-md-4">
												<h1>150</h1>
											</div>
											<hr>
										</div>
										<div class="row">
											<div class="col-md-8">
												<h1>Single</h1>
											</div>
											<div class="col-md-4">
												<h1>150</h1>
											</div>
											<hr>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

		
				</div>
			</div>

		</div>
		