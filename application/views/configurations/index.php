	<div class="container1">
		<div class="page-container">
			<!-- BEGIN SIDEBAR --><!-- END SIDEBAR -->
			   <?php
            $this->load->view('admin_template/leftmenu.php')?>
			<!-- BEGIN CONTENT -->
			
<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<div class="page-content">

					<h3 class="page-title">
						Configuration</h3>

					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								Setting
							</div>

						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form class="form-horizontal" action="#">
								<div class="form-body">

									<div class="form-group">
										<label class="col-md-3 control-label">User Id</label>
										<div class="col-md-4">
											<div class="input-group">

												<input type="text" placeholder="" disabled class="form-control input-circle-right">

											</div>
										</div>

									</div>



									<div class="form-group">
										<label class="col-md-3 control-label">New Password</label>
										<div class="col-md-4">
											<div class="input-group">

												<input type="text" placeholder="" class="form-control input-circle-right">

											</div>
										</div>

									</div>

									<div class="form-group">
										<label class="col-md-3 control-label">Confirm Password</label>
										<div class="col-md-4">
											<div class="input-group">

												<input type="text" placeholder="" class="form-control input-circle-right">

											</div>
										</div>

									</div>



								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-circle blue" type="submit">Submit</button>
											<button class="btn btn-circle default" type="button">Cancel</button>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>

					<br>
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">Configuration </div>

						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form class="form-horizontal" action="#">
								<div class="form-body">
									
									<h3 class="form-section">TAX Info
										<span class="pull-right" style="font-size:18px; display:inherit; margin-bottom:10px;">
											<input style="margin-top: 1px;" type="checkbox" name="tax" value="tax"> TAX Apply </span>
									</h3>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">CGST Tax</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">SGST Tax</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Services Tax</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Other Tax</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										<!--/span-->
										<!--/span-->
									</div>
									
									<hr>
									<h3 class="form-section">SMS Configuration
										<span class="pull-right" style="font-size:18px; display:inherit; margin-bottom:10px;">
											<input style="margin-top: 1px;" type="checkbox" name="tax" value="tax"> SMS Apply </span>
									</h3>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Phone Number</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Email ID</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
									</div>
									
									
									<hr>
									<h3 class="form-section">Bill Configuration
										<span class="pull-right" style="font-size:18px; display:inherit; margin-bottom:10px;">
											<input style="margin-top: 1px;" type="checkbox" name="tax" value="tax"> Bill Apply </span>
									</h3>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">12 P.M to 12 P.M</label>
												<div class="col-md-9">
													<input type="checkbox" name="tax" value="tax">
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Check-In</label>
												<div class="col-md-9">
													<input type="checkbox" name="tax" value="tax">
												</div>
											</div>
										</div>
									</div>

									<!--/row-->
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<button class="btn btn-circle blue" type="submit">Submit</button>
													<button class="btn btn-circle default" type="button">Cancel</button>
												</div>
											</div>
										</div>
										<div class="col-md-6">
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					
					
				</div>
			</div>

			</div>
		