	<div class="container1">
		<div class="page-container">
			<!-- BEGIN SIDEBAR --><!-- END SIDEBAR -->
			   <?php
            $this->load->view('admin_template/leftmenu.php')?>
			<!-- BEGIN CONTENT -->
			
<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<div class="page-content">

					<h3 class="page-title">
						Create Company</h3>

					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								Company
							</div>

						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form class="form-horizontal" action="#">
								<div class="form-body">

									<div class="form-group">
										<label class="col-md-3 control-label">Company Name</label>
										<div class="col-md-4">
											<div class="input-group">

												<input type="password" placeholder="" class="form-control input-circle-left">

											</div>
										</div>
									</div>


									<div class="form-group">
										<label class="col-md-3 control-label">City</label>
										<div class="col-md-4">
											<div class="input-group">

												<input type="text" placeholder="" class="form-control input-circle-right">

											</div>
										</div>

									</div>



								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-circle blue" type="submit">Submit</button>
											<button class="btn btn-circle default" type="button">Cancel</button>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>

				</div>
			</div>

			</div>
		