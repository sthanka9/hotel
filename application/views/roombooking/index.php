	<div class="container1">
		<div class="page-container">
			<!-- BEGIN SIDEBAR --><!-- END SIDEBAR -->
			   <?php
            $this->load->view('admin_template/leftmenu.php')?>
			<!-- BEGIN CONTENT -->
			
<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">Room Booking</div>

						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form class="form-horizontal" action="#">
								<div class="form-body">


									<h3 class="form-section">Booking Dates</h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">CHECK-IN</label>
												<div class="col-md-9">
													<div class='input-group date' id='check-in'>
														<input type='text' class="form-control" />
														<span class="input-group-addon">
															<span class="glyphicon glyphicon-calendar"></span>
														</span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">CHECK-OUT</label>
												<div class="col-md-9">
													<div class='input-group date' id='check-out'>
														<input type='text' class="form-control" />
														<span class="input-group-addon">
															<span class="glyphicon glyphicon-calendar"></span>
														</span>
													</div>
												</div>
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Referral</label>
												<div class="col-md-9">
													<select class="form-control">
														<option>Walkins </option>
														<option>Oyo</option>
														<option>Other</option>														
													</select>
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Advanced Booking</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>										
									</div>
									<hr>
									<h3 class="form-section">Room Type</h3>
									
									<div class="row">
										<div id="education_fields">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Room Type</label>
												<div class="col-md-9 ">
													<select class="form-control">
														<option value="Single">Single</option>
														<option value="Double">Double</option>
														<option value="Triple">Triple</option>
														<option value="Quad">Quad</option>
														<option value="Queen">Queen</option>
														<option value="King">King</option>
														<option value="Twin">Twin</option>
														<option value="Double-double">Double-double</option>
														<option value="Studio">Studio</option>
													</select>
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Room No.</label>
												<div class="col-md-7">
												<select class="form-control">
														<option value="123">123</option>
														<option value="234">234</option>
														<option value="345">345</option>
														<option value="456">456</option>
														<option value="567">567</option>
														<option value="765">765</option>
														<option value="235">235</option>
														
													</select>													
												</div>
												<div class="col-md-2 input-group-btn">
												<button class="btn btn-success" type="button"  onclick="education_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
												</div>
											</div>
										</div>
										 <div class="clearfix"> </div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"> No. Days</label>
												<div class="col-md-9">
													<select class="form-control">
														<option>1</option>
														<option>2</option>
														<option>3</option>
													</select>

												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"> No. Nights</label>
												<div class="col-md-9">
													<select class="form-control">
														<option>1</option>
														<option>2</option>
														<option>3</option>
													</select>
												</div>
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"> Rooms Cost</label>
												<div class="col-md-9">
													<input type="text" value="2500.00" disabled class="form-control">

												</div>
											</div>
										</div>

									</div>
									<hr>
									
									<h3 class="form-section">Persons</h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"> Adult(s)</label>
												<div class="col-md-9">
													<select class="form-control">
														<option>0</option>
														<option>1</option>
														<option>2</option>
														<option>3</option>
													</select>

												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Children</label>
												<div class="col-md-9">
													<select class="form-control">
														<option>0</option>
														<option>1</option>
														<option>2</option>
														<option>3</option>
													</select>
												</div>
											</div>
										</div>

									</div>

									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Family</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Friends</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Colleagues</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-3">Others</label>
												<div class="col-md-2">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
												<div class="col-md-7">
														<input type="text" class="form-control">
												</div>
											</div>
										</div>


									</div>

									<hr>

									<h3 class="form-section">Address</h3>
									<div class="row">
										
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Company Name</label>
												<div class="col-md-9">
													<input type="text" class="form-control">

												</div>
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Company Details</label>
												<div class="col-md-9">
													<input type="text" class="form-control">

												</div>
											</div>
										</div>
									</div>	
										<hr>
										<div class="clearfix"> </div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Titles</label>
												<div class="col-md-9">
													<select class="form-control">
														<option>Mr.</option>
														<option>Mrs</option>
														<option>Ms.</option>
														<option>Miss</option>
													</select>

												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Sex</label>
												<div class="col-md-9">
													<select class="form-control">
														<option>Male</option>
														<option>Femnale</option>
														<option>Other</option>														
													</select>

												</div>
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Frist Name</label>
												<div class="col-md-9">
													<input type="text" class="form-control">

												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Last Name</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"> Email</label>
												<div class="col-md-9">
													<input type="email" class="form-control">

												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Phone</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>

									</div>

									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Address 1</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Address 2</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">City</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">State</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Country</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Post Code</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Reason for Visit</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Next Destination</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										<!--/span-->

										<!--/span-->
									</div>
									<hr>
									<h3 class="form-section">ID Proofs</h3>
									<div class="row">

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Proof1</label>
												<div class="col-md-9">
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<span class="btn btn-default btn-file">
															<input type="file" />
														</span>
														<span class="fileinput-filename"></span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Proof2</label>
												<div class="col-md-9">
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<span class="btn btn-default btn-file">
															<input type="file" />
														</span>
														<span class="fileinput-filename"></span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<hr>
									<h3 class="form-section">Other Facilities</h3>
									<div class="row">

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Extra Bed</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Break fast</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Lunch</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Dinner</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Item1</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Item2</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>

									</div>
									<hr>
									<h3 class="form-section">Payment</h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Advance</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Payment Type</label>
												<div class="col-md-9">
													<select class="form-control">
														<option>Cash</option>
														<option>PAYTM</option>
														<option>Debit card</option>
														<option>Credit card</option>
														<option>Split Payment</option>
													</select>
												</div>
											</div>
										</div>
										<!--/span-->

										<!--/span-->
									</div>

									<!--/row-->
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<button class="btn btn-circle blue" type="submit">Submit</button>
													<button class="btn btn-circle default" type="button">Cancel</button>
												</div>
											</div>
										</div>
										<div class="col-md-6">
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>

				</div>
			</div>

			</div>
		