	<div class="container1">
		<div class="page-container">
			<!-- BEGIN SIDEBAR --><!-- END SIDEBAR -->
			   <?php
            $this->load->view('admin_template/leftmenu.php')?>
			<!-- BEGIN CONTENT -->
			
<!-- BEGIN CONTENT -->



			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">Modify Booking</div>

						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form class="form-horizontal" action="#">
								<div class="form-body">

									<h3 class="form-section">Other Facilities</h3>
									<div class="row">

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Extra Bed</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Break fast</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Lunch</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Dinner</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Item1</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Item2</label>
												<div class="col-md-3">
													<input type="checkbox" name="vehicle" value="Bike">
												</div>
											</div>
										</div>

									</div>

									<hr>

									<h3 class="form-section">Add Room</h3>
									<div class="row">


										<div id="education_fields">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Room Type</label>
												<div class="col-md-9 ">
													<select class="form-control">
														<option value="Single">Single</option>
														<option value="Double">Double</option>
														<option value="Triple">Triple</option>
														<option value="Quad">Quad</option>
														<option value="Queen">Queen</option>
														<option value="King">King</option>
														<option value="Twin">Twin</option>
														<option value="Double-double">Double-double</option>
														<option value="Studio">Studio</option>
													</select>
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Room No.</label>
												<div class="col-md-7">
												<select class="form-control">
														<option value="123">123</option>
														<option value="234">234</option>
														<option value="345">345</option>
														<option value="456">456</option>
														<option value="567">567</option>
														<option value="765">765</option>
														<option value="235">235</option>
														
													</select>													
												</div>
												<div class="col-md-2 input-group-btn">
												<button class="btn btn-success" type="button"  onclick="education_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
												</div>
											</div>
										</div>
										 <div class="clearfix"> </div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"> No Days</label>
												<div class="col-md-9">
													<select class="form-control">
														<option>1</option>
														<option>2</option>
														<option>3</option>
													</select>

												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"> No. Nights</label>
												<div class="col-md-7">
													<select class="form-control">
														<option>1</option>
														<option>2</option>
														<option>3</option>
													</select>
												</div>
											</div>
										</div>


									</div>
									<hr>
									<h3 class="form-section">Change Rooms</h3>
									<div class="row">

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Current Room</label>
												<div class="col-md-9">
													<input type="text" disabled class="form-control">

												</div>
											</div>
										</div>


										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"> Room No</label>
												<div class="col-md-9">
													<select class="form-control">

														<option>1</option>
														<option>2</option>
														<option>3</option>
													</select>

												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Reason for Change</label>
												<div class="col-md-9">
													<textarea class="form-control" name="message" rows="4" cols="30">
																	</textarea>
												</div>
											</div>
										</div>

									</div>


									<!--/row-->
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<button class="btn btn-circle blue" type="submit">Submit</button>
													<button class="btn btn-circle default" type="button">Cancel</button>
												</div>
											</div>
										</div>
										<div class="col-md-6">
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>

				</div>
			</div>


			</div>
		