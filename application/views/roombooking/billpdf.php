	<div class="container1">
		<div class="page-container">
			<!-- BEGIN SIDEBAR --><!-- END SIDEBAR -->
			   <?php
            $this->load->view('admin_template/leftmenu.php')?>
			<!-- BEGIN CONTENT -->
			
<!-- BEGIN CONTENT -->



			<div class="page-content-wrapper">
				<div class="page-content">

					<h3 class="page-title">
						Bill Print</h3>

					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								Print Invoice
							</div>

						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form class="form-horizontal" action="#">
								<div class="form-body">


									<table align="center" cellpadding="0" cellspacing="0" id="printTable" style="font-weight:normal; width: 80%;">
										<thead>
											<tr>
												<th width="" colspan="4">
													<div align="center">
														<h3>Hotel Royal Plaza</h3>
														#NH-44, Near RGI Police Station, Siddanthi Shamshabad, Hyderabad, R.R Dist, Telangana
														<br/> Conttact : +91-9666692655, Email : royalplazahyd@gmail.com </div>
													<hr>
												</th>
											</tr>
											<tr>
												<th colspan="4">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="50%" height="30">Invoice : 12345678
																<hr>
															</td>
															<td width="50%" height="30">
																<div align="right">Order Date : 02-11-2017, 22:30</div>
																<hr>
															</td>
														</tr>
													</table>
												</th>
											</tr>
											<tr>
												<th colspan="4">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td height="30">
																<div align="left">Particulars</div>
																<hr>
															</td>
															<td width="14%" height="30">
																<div align="right">Days</div>
																<hr>
															</td>
															<td width="21%" height="30">
																<div align="right">Rate</div>
																<hr>
															</td>
															<td width="29%" height="30">
																<div align="right">Total</div>
																<hr>
															</td>
														</tr>
														<tr>
															<td width="36%" height="35">
																<div align="left">Single Room</div>
															</td>
															<td height="35">
																<div align="right">3</div>
															</td>
															<td height="35">
																<div align="right">1500.00</div>
															</td>
															<td height="35">
																<div align="right">4500.00</div>
															</td>
														</tr>
													</table>
												</th>
											</tr>
											<tr>
												<th colspan="4">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">

														<tr>
															<td width="100%" colspan="2" height="30">
																<hr>
																<div align="center">Other Services</div>
															</td>

														</tr>
														<tr>
															<td width="71%" height="30">
																<hr>
																<div align="right">Food</div>
															</td>
															<td width="29%" height="30">
																<hr>
																<div align="right">500.00 </div>
															</td>
														</tr>
														<tr>
															<td width="71%" height="30">
																<div align="right">Facilities</div>
															</td>
															<td width="29%" height="30">
																<div align="right">250.00 </div>
															</td>
														</tr>

														<tr>
															<td width="71%" height="50">
																<hr>
																<div align="right">Total Amount</div>
																<hr>
															</td>
															<td width="29%" height="50">
																<hr>
																<div align="right">4500.00 </div>
																<hr>
															</td>
														</tr>
														<tr ng-if="orderDetails.discount_amount">
															<td width="71%" height="35">
																<div align="right">Discount</div>
															</td>
															<td width="29%" height="35">
																<div align="right">500.0</div>
															</td>
														</tr>
														<tr ng-if="orderDetails.discount_amount">
															<td width="71%" height="30">
																<div align="right">Total</div>
															</td>
															<td width="29%" height="30">
																<div align="right">4000.00</div>
															</td>
														</tr>
														<tr class="green-strip1">
															<td height="30">
																<div align="right">CGST
																	<span ng-if="tax.type =='percentage'"> @ 8%</span>
																</div>
															</td>
															<td height="30">
																<div align="right">360</div>
															</td>
														</tr>
														<tr class="green-strip1">
															<td height="30">
																<div align="right">SGST
																	<span ng-if="tax.type =='percentage'"> @ 8%</span>
																</div>
															</td>
															<td height="30">
																<div align="right">360</div>
															</td>
														</tr>
														<tr ng-if="getRoundOfValue(orderDetails)">
															<td width="71%" height="30">
																<div align="right">Round Off</div>
															</td>
															<td width="29%" height="30">
																<div align="right">0.8</div>
															</td>
														</tr>
														<tr>
															<td width="71%" height="30">
																<div align="right">Grand (Rounded) Total</div>
															</td>
															<td width="29%" height="30">
																<div align="right">6000 </div>
															</td>
														</tr>
													</table>
												</th>
											</tr>
											<tr>
												<th height="30" colspan="4">
													<hr>
													<h2 align="center" style="margin-top:5px;">Rs.6000 </h2>
												</th>
											</tr>
											<tr>
												<th height="30" colspan="2" valign="bottom">
													<div align="left">GST: GSTNumber</div>
												</th>
												<th height="30" colspan="2" valign="bottom">
													<div align="right">Table: Room No.</div>
												</th>
											</tr>
											<tr>
												<th height="30" colspan="4" valign="bottom">
													<div align="center">
														<h4>Thank you please visit again</h4>
													</div>
												</th>
											</tr>
										</thead>
									</table>







								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-6 col-md-6">
											<button class="btn btn-circle blue" type="submit">Print</button>

										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>

				</div>
			</div>


			</div>
		