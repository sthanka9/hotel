	<div class="container1">
		<div class="page-container">
			<!-- BEGIN SIDEBAR --><!-- END SIDEBAR -->
			   <?php
            $this->load->view('admin_template/leftmenu.php')?>
			<!-- BEGIN CONTENT -->
			
<!-- BEGIN CONTENT -->



			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">Bill Generate</div>

						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form class="form-horizontal" action="#">
								<div class="form-body">


									<h3 class="form-section">Booking Dates</h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">CHECK-IN</label>
												<div class="col-md-9">
													<input type="date" disabled class="form-control">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">CHECK-OUT</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
									</div>
									<hr>
									<h3 class="form-section">Rooms</h3>
									<div class="row">


										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Room Type</label>
												<div class="col-md-9">
													<input type="text" value="Single, Double" disabled class="form-control">

												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Room No.</label>
												<div class="col-md-9">
													<input type="text" value="103, 104" disabled class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"> Rooms Qty</label>
												<div class="col-md-9">
													<input type="text" value="2" disabled class="form-control">

												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"> Rooms Cost</label>
												<div class="col-md-9">
													<input type="text" value="2500.00" disabled class="form-control">

												</div>
											</div>
										</div>

									</div>

									<hr>
									<h3 class="form-section">Other Facilities</h3>
									<div class="row">

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Extra Bed</label>
												<div class="col-md-6">
													<input type="text" value="250.00" disabled class="form-control">
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Break fast</label>
												<div class="col-md-6">
													<input type="text" value="250.00" disabled class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Lunch</label>
												<div class="col-md-6">
													<input type="text" value="250.00" disabled class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Dinner</label>
												<div class="col-md-6">
													<input type="text" value="250.00" disabled class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Item1</label>
												<div class="col-md-6">
													<input type="text" value="250.00" disabled class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-6">Item2</label>
												<div class="col-md-6">
													<input type="text" value="250.00" disabled class="form-control">
												</div>
											</div>
										</div>

									</div>
									<hr>
									<h3 class="form-section">Mode of Payment</h3>
									<div class="row">
									
									<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Room Cost</label>
												<div class="col-md-9">
													<input type="text" disabled class="form-control">
												</div>
											</div>
										</div>
										
										
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Advanced Amount</label>
												<div class="col-md-9">
													<input type="text" disabled class="form-control">
												</div>
											</div>
										</div>

										

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Facilities Amount</label>
												<div class="col-md-9">
													<input type="text" disabled class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Discount Amount</label>
												<div class="col-md-9">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Total Bill Amount</label>
												<div class="col-md-9">
													<input type="text" disabled class="form-control">
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Payment Type</label>
												<div class="col-md-9">
													<select class="form-control">
														<option>Cash</option>
														<option>PAYTM</option>
														<option>Debit card</option>
														<option>Credit card</option>
														<option>Split payment</option>														
													</select>
												</div>
											</div>
										</div>
										<!--/span-->

										<!--/span-->
									</div>

									<!--/row-->
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<button class="btn btn-circle blue" type="submit">Submit</button>
													<button class="btn btn-circle default" type="button">Cancel</button>
												</div>
											</div>
										</div>
										<div class="col-md-6">
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>

				</div>
			</div>


			</div>
		